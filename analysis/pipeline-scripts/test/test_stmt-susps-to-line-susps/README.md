This directory contains tests for the `stmt-susps-to-line-susps` python script,
which is part of the analysis pipeline. In particular, it contains a test script
(`test_stmt-susps-to-line-susps.sh`) and a set of test cases.

Run `test_stmt-susps-to-line-susps.sh` to run all test cases.
