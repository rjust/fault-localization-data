#!/usr/bin/env bash
#
# ------------------------------------------------------------------------------
# This script collects all Defects4J real faults for which Major is not able to
# generate any mutant for any buggy line. The output file follows the following
# format:
#
#  pid,bid
#
# Where <pid> is the project name and <bid> the real fault id of the artificial
# fault.
#
# For example:
#  pid,bid,muse_score,metallaxis_score,sbfl_score
#  ...
#  Chart,24,0.76,0.75,0.45
#
# Usage:
# ./get-faults-with-no-mutated-buggy-lines.sh <output_file>
#
# Requirements:
# - The environment variable D4J_HOME needs to be set and must point to the
#   Defects4J installation.
# - data/mutants_per_line.csv file with all mutated lines per Defects4J fault.
#
# ------------------------------------------------------------------------------

HERE=$(cd `dirname ${BASH_SOURCE[0]}` && pwd)

#
# Print error message and exit
#
die() {
  echo "$@" >&2
  exit 1
}

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"

MUTANTS_PER_LINE="$HERE/../../data/mutants_per_line.csv"
[ -s "$MUTANTS_PER_LINE" ] || die "$MUTANTS_PER_LINE does not exist or it is empty!"

BLACKLIST_FILE="$HERE/../../data/blacklist.csv"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE does not exist or it is empty!"

FL_SCORES_FILE="$HERE/../../data/scores_artificial_vs_real.csv"
[ -s "$FL_SCORES_FILE" ] || die "$FL_SCORES_FILE does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} <output_file>"
[ "$#" -eq "1" ] || die "$USAGE"

OUTPUT_FILE="$1"
echo "pid,bid,mbfl_muse_score,mbfl_metallaxis_score,sbfl_ochiai_score" > "$OUTPUT_FILE" || die "Cannot write to $OUTPUT_FILE!"

# ------------------------------------------------------------- Helper functions

_check_line() {
  local USAGE="Usage: ${FUNCNAME[0]} <pib> <bid> <buggy_line>"
  if [ "$#" != 3 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local pid="$1"
  local bid="$2"
  local buggy_line="$3"
  echo "[DEBUG] Checking if '$buggy_line' of $pid-$bid has been mutated!"

  local classname=$(echo "$buggy_line" | cut -f1 -d'#' | sed 's/.java$//' | sed 's|/|.|g')
  [ "$classname" != "" ] || die "classname variable is empty!"

  local line_number=$(echo "$buggy_line" | cut -f2 -d'#')
  [ "$line_number" != "" ] || die "classname variable is empty!"
  echo "  [DEBUG] classname: '$classname' | line_number: '$line_number'!"

  ##
  # Main class
  if grep -q "^$pid,$bid,.*,$classname:$line_number$" "$MUTANTS_PER_LINE"; then
    echo "  [DEBUG] Yes, it has been mutated!"
    return 0
  fi
  if grep -q "^$pid,$bid,.*,$classname@.*:$line_number$" "$MUTANTS_PER_LINE"; then
    # Maybe the buggy line is in a method
    echo "  [DEBUG] Yes, it has been mutated and it is in a method!"
    return 0
  fi

  ##
  # Inner class
  if grep -q "^$pid,$bid,.*,$classname\$.*:$line_number$" "$MUTANTS_PER_LINE"; then
    echo "  [DEBUG] Yes, it has been mutated and it is in an inner class!"
    return 0
  fi
  if grep -q "^$pid,$bid,.*,$classname\$.*@.*:$line_number$" "$MUTANTS_PER_LINE"; then
    # Maybe the buggy line is in a method
    echo "  [DEBUG] Yes, it has been mutated and it is in a method of a inner class!"
    return 0
  fi

  return 1
}

# ------------------------------------------------------------------------- Main

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"
  [ -d "$dir_project" ] || die "$dir_project does not exist!"

  for bid in $(cut -f1 -d',' "$dir_project/commit-db"); do
    if [ "$bid" -gt "1000" ]; then
      # skip artificial faults
      break
    fi

    echo "$pid-$bid"

    buggy_lines_file="$HERE/../../analysis/pipeline-scripts/buggy-lines/$pid-$bid.buggy.lines"
    [ -f "$buggy_lines_file" ] || die "$buggy_lines_file does not exist!"
    candidate_buggy_lines_file="$HERE/../../analysis/pipeline-scripts/buggy-lines/$pid-$bid.candidates"

    has_found_it=false
    source_code_lines_file="$HERE/../../analysis/pipeline-scripts/source-code-lines/$pid-${bid}b.source-code.lines"

    #
    # Check list of buggy lines
    #
    while read -r line; do
      java_file=$(echo "$line" | cut -f1 -d'#')
      [ "$java_file" != "" ] || die "java_file variable is empty!"
      line_number=$(echo "$line" | cut -f2 -d'#')
      [ "$line_number" != "" ] || die "line_number variable is empty!"
      buggy_code=$(echo "$line" | cut -f3 -d'#')
      [ "$buggy_code" != "" ] || die "buggy_code variable is empty!"

      buggy_line="$java_file#$line_number"
      _check_line "$pid" "$bid" "$buggy_line"
      if [ $? -eq 0 ]; then
        has_found_it=true
        break
      fi

      ##
      # Is it part of a multi line?
      if [ -s "$source_code_lines_file" ]; then
        while read -r source_code_line; do
          _check_line "$pid" "$bid" "$source_code_line"
          if [ $? -eq 0 ]; then
            has_found_it=true
            break
          fi
        done < <(grep ":$buggy_line$" "$source_code_lines_file" | cut -f1 -d':')
        while read -r source_code_line; do
          _check_line "$pid" "$bid" "$source_code_line"
          if [ $? -eq 0 ]; then
            has_found_it=true
            break
          fi
        done < <(grep "^$buggy_line:" "$source_code_lines_file" | cut -f2 -d':')
      fi
    done < <(cat "$buggy_lines_file" | grep -v "#FAULT_OF_OMISSION")

    #
    # Before giving up, check list of candidate buggy lines
    #
    if [[ $has_found_it == false ]]; then
      if [ -s "$candidate_buggy_lines_file" ]; then
        while read -r buggy_line; do
          _check_line "$pid" "$bid" "$buggy_line"
          if [ $? -eq 0 ]; then
            has_found_it=true
            break
          fi

          ##
          # Is it part of a multi line?
          if [ -s "$source_code_lines_file" ]; then
            while read -r source_code_line; do
              _check_line "$pid" "$bid" "$source_code_line"
              if [ $? -eq 0 ]; then
                has_found_it=true
                break
              fi
            done < <(grep ":$buggy_line$" "$source_code_lines_file" | cut -f1 -d':')
            while read -r source_code_line; do
              _check_line "$pid" "$bid" "$source_code_line"
              if [ $? -eq 0 ]; then
                has_found_it=true
                break
              fi
            done < <(grep "^$buggy_line:" "$source_code_lines_file" | cut -f2 -d':')
          fi
        done < <(cat "$candidate_buggy_lines_file" | cut -f2 -d',' | sort -u)
      fi
    fi

    if [[ $has_found_it == false ]]; then
      # Get MUSE score
      mbfl_muse_score=$(grep "^$pid,$bid,.*,mbfl,muse," "$FL_SCORES_FILE" | tr -d '\r' | cut -f12 -d',')
      # Get Metallaxis score
      mbfl_metallaxis_score=$(grep "^$pid,$bid,.*,mbfl,ochiai," "$FL_SCORES_FILE" | tr -d '\r' | cut -f12 -d',')
      # Get SBFL score
      sbfl_ochiai_score=$(grep "^$pid,$bid,.*,sbfl,ochiai," "$FL_SCORES_FILE" | tr -d '\r' | cut -f12 -d',')

      echo "$pid,$bid,$mbfl_muse_score,$mbfl_metallaxis_score,$sbfl_ochiai_score" >> "$OUTPUT_FILE"
    fi
  done
done

echo "DONE!"
exit 0
