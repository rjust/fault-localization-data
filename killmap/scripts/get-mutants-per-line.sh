#!/usr/bin/env bash
#
# ------------------------------------------------------------------------------
# This script collects all mutated lines per project/bug. The output file
# follows the following format:
#
#  pid,bid,mid,operator,line
#
# Where <pid> is the project name, <bid> the real fault id of the artificial
# fault, <mid> the artificial fault id, and <line> the mutate line
# (class:line_number)
#
# For example:
#  pid,bid,operator,line
#  ...
#  Chart,24,AOR,org.jfree.chart.renderer.GrayPaintScale:126
#  Chart,24,AOR,org.jfree.chart.renderer.GrayPaintScale:126
#  Chart,25,ROR,org.jfree.chart.renderer.category.StatisticalBarRenderer:259
#
# Usage:
# ./get-mutants-per-line.sh <output_file>
#
# Requirements:
# - The environment variable D4J_HOME needs to be set and must point to the
#   Defects4J installation.
# - KILLMAPS_FILES_DIR   Needs to be set and must point to KillMap tar.gz files.
#
# ------------------------------------------------------------------------------

HERE=$(cd `dirname ${BASH_SOURCE[0]}` && pwd)

#
# Print error message and exit
#
die() {
  echo "$@" >&2
  exit 1
}

# ------------------------------------------------------------------ Envs & Args

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "[ERROR] D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"

# Check whether KILLMAPS_FILES_DIR is set
[ "$KILLMAPS_FILES_DIR" != "" ] || die "KILLMAPS_FILES_DIR is not set!"
[ -d "$KILLMAPS_FILES_DIR" ] || die "$KILLMAPS_FILES_DIR does not exist!"

BLACKLIST_FILE="$HERE/../../data/blacklist.csv"
[ -s "$BLACKLIST_FILE" ] || die "$BLACKLIST_FILE does not exist or it is empty!"

USAGE="Usage: ${BASH_SOURCE[0]} <output_file>"
[ "$#" -eq "1" ] || die "$USAGE"

OUTPUT_FILE="$1"
echo "pid,bid,operator,line" > "$OUTPUT_FILE" || die "Cannot write to $OUTPUT_FILE!"

# ------------------------------------------------------------------------- Main

TMP_DIR="/scratch/$$-get-mutants-per-line"

for pid in Chart Closure Lang Math Mockito Time; do
  dir_project="$D4J_HOME/framework/projects/$pid"
  [ -d "$dir_project" ] || die "$dir_project does not exist!"

  for bid in $(cut -f1 -d',' "$dir_project/commit-db"); do
    if [ "$bid" -gt "1000" ]; then
      # skip artificial faults
      break
    fi

    echo "$pid-$bid"

    #
    # Unpack Killmap file
    #

    killmap_file="$KILLMAPS_FILES_DIR/$pid/$bid/168h-killmap-files.tar.gz"
    if [ ! -s "$killmap_file" ]; then
      killmap_file="$KILLMAPS_FILES_DIR/$pid/$bid/32h-killmap-files.tar.gz"
      if [ ! -s "$killmap_file" ]; then
        rm -rf "$TMP_DIR"
        die "[ERROR] Neither 32h-killmap-files.tar.gz or 168h-killmap-files.tar.gz exist!"
      fi
    fi

    mutants_dir="$TMP_DIR/$pid"
    mkdir -p "$mutants_dir"
    tar -xzf "$killmap_file" -C "$mutants_dir"
    if [ $? -ne 0 ]; then
      rm -rf "$TMP_DIR"
      die "[ERROR] Extraction of '$killmap_file' has failed!"
    fi

    mutants_log_file="$mutants_dir/killmaps/$pid/$bid/mutants.log"
    if [ ! -s "$mutants_log_file" ]; then
      rm -rf "$TMP_DIR"
      die "[ERROR] $mutants_log_file does not exist or it is empty!"
    fi

    cat "$mutants_log_file" | sed -E 's/^[0-9]+:([A-Z]{3}):.*:.*:(.*:[0-9]+):.*$/\1:\2/' | sed 's/:/,/' | \
      sed "s/^/$pid,$bid,/" >> "$OUTPUT_FILE"
    if [ $? -ne 0 ]; then
      rm -rf "$TMP_DIR"
      die "[ERROR] Collection of data has failed!"
    fi

    rm -rf "$mutants_dir"
  done
done

rm -rf "$TMP_DIR" # Clean up temporary data

echo "DONE!"
exit 0
