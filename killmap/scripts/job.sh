#!/usr/bin/env bash
# ------------------------------------------------------------------------------
# 
# This script runs Killmap on a specified D4J project/bug using the manually
# written test cases.
# 
# Usage:
# ./job.sh
#   --project <project_name>
#   --bug <bug_id>
#   --output_dir <path>
#   --log_file <path>
#   [--timeout <hours, 8 by default>]
#   [--partial_output <path, /dev/null by default>]
#   [--help]
# 
# Environment variables:
# - D4J_HOME       Needs to be set and must point to the Defects4J installation.
# 
# ------------------------------------------------------------------------------

SCRIPT_DIR=`pwd`
source "$SCRIPT_DIR/utils.sh" || exit 1
source "$SCRIPT_DIR/../../gzoltar/utils.sh" || exit 1

if _am_I_a_cluster; then
  module load apps/python/2.7 || die "Failed to load python 2.7.2!"
fi

# ------------------------------------------------------------------ Envs & Args

export PYTHONPATH="$SCRIPT_DIR/../../utils:$PYTHONPATH"

# Check whether D4J_HOME is set
[ "$D4J_HOME" != "" ] || die "D4J_HOME is not set!"
[ -d "$D4J_HOME" ] || die "$D4J_HOME does not exist!"
export DEFECTS4J_HOME="$D4J_HOME"

USAGE="Usage: ${BASH_SOURCE[0]} --project <project_name> --bug <bug_id> --output_dir <path> --log_file <path> [--timeout <hours, 8 by default>] [--partial_output <path, /dev/null by default>] [help]"
if [ "$#" -ne "1" ] && [ "$#" -gt "12" ]; then
  die "$USAGE"
fi

PID=""
BID=""
OUTPUT_DIR=""
LOG_FILE=""
TIMEOUT="08"
PARTIAL_OUTPUT="/dev/null"

while [[ "$1" = --* ]]; do
  OPTION=$1; shift
  case $OPTION in
    (--project)
      PID=$1;
      shift;;
    (--bug)
      BID=$1;
      shift;;
    (--output_dir)
      OUTPUT_DIR=$1;
      shift;;
    (--timeout)
      TIMEOUT=$1;
      shift;;
    (--log_file)
      LOG_FILE=$1;
      shift;;
    (--partial_output)
      PARTIAL_OUTPUT=$1;
      shift;;
    (--help)
      echo "$USAGE"
      exit 0;;
    (*)
      die "$USAGE";;
  esac
done

[ "$PID" != "" ] || die "$USAGE"
[ "$BID" != "" ] || die "$USAGE"

[ "$OUTPUT_DIR" != "" ] || die "$USAGE"
[ -d "$OUTPUT_DIR" ] || mkdir -p "$OUTPUT_DIR"

[ "$TIMEOUT" != "" ] || die "$USAGE"
TIMEOUT=$(echo "$TIMEOUT" | sed 's/^0*//') # to avoid bash interpreting, e.g., 08, as an octal number
# Subtract one hour to the total timeout as there are few other tasks to perform
# (i.e., checkout, compilation, etc)
TIMEOUT=$((TIMEOUT-1))

[ "$LOG_FILE" != "" ] || die "$USAGE"
[ -f "$LOG_FILE" ] || die "$LOG_FILE does not exist!"

LOCAL_TMP_DIR="/tmp/$USER-$PID-$BID-$$"
rm -rf "$LOCAL_TMP_DIR"
mkdir -p "$LOCAL_TMP_DIR" || die "Failed to create temporary directory '$LOCAL_TMP_DIR'"

LOCAL_CHECKOUT_DIR="$LOCAL_TMP_DIR/checkout/$PID/$BID"
mkdir -p "$LOCAL_CHECKOUT_DIR" || die

LOCAL_DATA_DIR="$LOCAL_TMP_DIR/killmaps/$PID/$BID"
mkdir -p "$LOCAL_DATA_DIR" || die

KILLMAP_CSV_FILE="$LOCAL_DATA_DIR/killmap.csv"
KILLMAP_GZ_FILE="$LOCAL_DATA_DIR/killmap.csv.gz"
MUTANTS_LOG_FILE="$LOCAL_DATA_DIR/mutants.log"

# -------------------------------------------------------------------------- Aux

_skip_killmap_sanity_check() {
  local USAGE="Usage: ${FUNCNAME[0]} <pid> <bid>"
  if [ "$#" != 2 ]; then
    echo "$USAGE" >&2
    return 1
  fi

  local pid="$1"
  local bid="$2"

  local relevant_tests_of_loaded_classes_file="$DEFECTS4J_HOME/framework/projects/$pid/relevant_tests_of_loaded_classes/$bid"
  [ -s "$relevant_tests_of_loaded_classes_file" ] || die "$relevant_tests_of_loaded_classes_file is empty or does not exist!"

  if grep -q "^org.jfree.chart.axis.junit.CategoryAxisTests$" "$relevant_tests_of_loaded_classes_file" && grep -q "^org.jfree.chart.axis.junit.SubCategoryAxisTests$"  "$relevant_tests_of_loaded_classes_file"; then
    # Skip sanity check for faults (e.g., Chart-26) with the following test
    # suites:
    #    org.jfree.chart.axis.junit.CategoryAxisTests
    #    org.jfree.chart.axis.junit.SubCategoryAxisTests
    #
    # The test suite 'org.jfree.chart.axis.junit.SubCategoryAxisTests' does not
    # run any of its own test cases. Instead, it runs the test cases from the
    # 'org.jfree.chart.axis.junit.CategoryAxisTests' test suite. The reason is
    # that rather than implementing
    #    return new TestSuite(SubCategoryAxisTests.class);
    # the test suite 'org.jfree.chart.axis.junit.SubCategoryAxisTests' implements
    #    return new TestSuite(CategoryAxisTests.class);
    #
    # Thus, the test cases of the 'org.jfree.chart.axis.junit.CategoryAxisTests'
    # suite are executed twice: first when 'CategoryAxisTests' is executed and
    # then when 'SubCategoryAxisTests' is executed. Therefore, the sanity check
    # will report a duplicate execution of the same test case(s) and return a
    # fail error.
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "600010" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "600012" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "1300012" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "2200002" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3200011" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300041" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300043" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300044" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300046" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300048" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300049" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300050" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3300052" ]; then
    return 0
  elif [ "$pid" == "Mockito" ] && [ "$bid" -eq "3400048" ]; then
    return 0
  fi

  return 1
}

# ------------------------------------------------------------------------- Main

cd "$SCRIPT_DIR" || die "Failed to move to '$SCRIPT_DIR'!"

echo "PID: $$"
hostname
java -version
python --version

#
# Generate killmap
#

echo ""
echo "[INFO] Run killmap on $PID-${BID}b"

pushd . > /dev/null 2>&1
cd "$SCRIPT_DIR" || die "Failed to move to '$SCRIPT_DIR'!"

  timeout --signal=KILL "${TIMEOUT}h" ./generate-matrix.sh \
    --partial-output "$PARTIAL_OUTPUT" \
    "$PID" "$BID" "$LOCAL_CHECKOUT_DIR" "$MUTANTS_LOG_FILE" \
    2>>$LOG_FILE > "$KILLMAP_CSV_FILE"
  ret="$?"
  if [ "$ret" -eq "0" ]; then
    echo "Execution of the 'generate-matrix.sh' script finished successfully!"
    rm -rf "$LOCAL_CHECKOUT_DIR"
  elif [ "$ret" -eq "1" ]; then
    rm -rf "$LOCAL_TMP_DIR"
    die "Failed to generate a killmap matrix!"
  elif [ "$ret" -eq "137" ]; then
    echo "Execution of the 'generate-matrix.sh' script ran out of time!"

    # remove last line of killmap.csv file as it might be incomplete (next
    # invocation of the 'generate-matrix.sh' script will regenerate that line)
    head -n -1 "$KILLMAP_CSV_FILE" > "$KILLMAP_CSV_FILE.tmp" && mv "$KILLMAP_CSV_FILE.tmp" "$KILLMAP_CSV_FILE"

    echo "Copying all files from local node '$LOCAL_DATA_DIR' to remote '$OUTPUT_DIR' so that a new invocation of the 'generate-matrix.sh' script could continue the job ..."
    cp -Rv $LOCAL_DATA_DIR/* "$OUTPUT_DIR/"
    rm -rf "$LOCAL_TMP_DIR"
    exit 1
  else
    echo "Something went wrong ($ret)!"
    echo "Copying all files from local node '$LOCAL_DATA_DIR' to remote '$OUTPUT_DIR' so that anyone can debug them"
    cp -Rv $LOCAL_DATA_DIR/* "$OUTPUT_DIR/"
    rm -rf "$LOCAL_TMP_DIR"
    exit 1
  fi

  if [ ! -s "$KILLMAP_CSV_FILE" ]; then
    rm -rf "$LOCAL_TMP_DIR"
    die "Killmap file does not exist or it is empty!"
  fi

  if ! grep -q "^Completed successfully\!$" "$LOG_FILE"; then
    rm -rf "$LOCAL_TMP_DIR"
    die "Generation of killmap matrix did not finish successfully!"
  fi

popd > /dev/null 2>&1

#
# Compress killmap
#

echo ""
echo "[INFO] Compress killmap matrix"

pushd . > /dev/null 2>&1
cd "$LOCAL_DATA_DIR"
  gzip "$KILLMAP_CSV_FILE"
  if [ $? -ne 0 ]; then
    rm -rf "$LOCAL_TMP_DIR"
    die "Failed to compress killmap matrix!"
  fi

  if [ ! -s "$KILLMAP_GZ_FILE" ]; then
    rm -rf "$LOCAL_TMP_DIR"
    die "Compressed killmap file does not exist or it is empty!"
  fi

  if grep -q " No space left on device$" "$LOG_FILE"; then
    rm -rf "$LOCAL_TMP_DIR"
    die "Local node ran out of space!"
  fi
popd > /dev/null 2>&1

#
# Sanity check
#

echo ""
echo "[INFO] Perform sanity checks on the generated killmap matrix"

_skip_killmap_sanity_check "$PID" "$BID"
skip_killmap_sanity_check="$?" # 0 yes, 1 no

if [ "$skip_killmap_sanity_check" == "0" ]; then
  echo "[INFO] It is a known exception therefore sanity check is not performed."
else
  pushd . > /dev/null 2>&1
  cd "$SCRIPT_DIR" || die "Failed to move to '$SCRIPT_DIR'!"

    python sanity-check "$KILLMAP_GZ_FILE"
    if [ $? -ne 0 ]; then
      echo "Failed to check whether the killmap matrix looks reasonable!"
      echo "Copying all files from local node '$LOCAL_DATA_DIR' to remote '$OUTPUT_DIR' so that anyone can debug them"
      cp -Rv $LOCAL_DATA_DIR/* "$OUTPUT_DIR/"
      rm -rf "$LOCAL_TMP_DIR"
      exit 1
    fi

  popd > /dev/null 2>&1
fi

#
# Collect and compress data
#

echo ""
echo "[INFO] Collect data & Clean up"

pushd . > /dev/null 2>&1
cd "$LOCAL_TMP_DIR"
  echo "DONE!"

  # get log file so that it can also be in the .tar.gz file
  cp -f "$OUTPUT_DIR/log.txt" "killmaps/$PID/$BID/" > /dev/null 2>&1

  zip_filename="killmap-files.tar.gz"
  tar -czf "$zip_filename" "killmaps/$PID/$BID/killmap.csv.gz" \
                           "killmaps/$PID/$BID/mutants.log" \
                           "killmaps/$PID/$BID/log.txt"
  if [ $? -ne 0 ]; then
    echo "It was not possible to compress directory '$LOCAL_TMP_DIR/killmaps/'!"

    echo "Copying all files from local node '$LOCAL_DATA_DIR' to remote '$OUTPUT_DIR' so that anyone can debug them"
    cp -Rv $LOCAL_DATA_DIR/* "$OUTPUT_DIR/"

    popd > /dev/null 2>&1
    rm -rf "$LOCAL_TMP_DIR"
    exit 1
  fi

  cp "$zip_filename" "$OUTPUT_DIR/" > /dev/null 2>&1
  rm -f "$OUTPUT_DIR/log.txt" > /dev/null 2>&1
popd > /dev/null 2>&1

rm -rf "$LOCAL_TMP_DIR" > /dev/null 2>&1 # Clean up
exit 0
