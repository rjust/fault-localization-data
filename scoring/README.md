# Score

## Run pipeline and score each Defects4J fault

* `jobs.sh `script runs the [pipeline-scripts](../analysis/pipeline-scripts) on
all Defects4J faults.

#### Requirements

* D4J_HOME - Must point to the Defects4J installation.
* GZOLTAR_FILES_DIR - Needs to be set and must point to GZoltar's tar.gz files.
* KILLMAPS_FILES_DIR - Needs to be set and must point to KillMap tar.gz files.

Directory structure of `GZOLTAR_FILES_DIR` should follow the following format:
`GZOLTAR_FILES_DIR/<project_name>/<bug_id>/gzoltar-files.tar.gz`. Directory
structure of `KILLMAPS_FILES_DIR` should follow the following format:
`KILLMAPS_FILES_DIR/<project_name>/<bug_id>/<32|168h>-killmap-files.tar.gz`.

#### Usage

```
  ./jobs.sh
    --output_dir <output_dir_path>
    [help]
```
Where `<output_dir_path>` is the directory to where the intermediate scoring
files will be written.

To runs the [pipeline-scripts](../analysis/pipeline-scripts) on each Defects4J
fault, `jobs.sh` script executes `job.sh` either as a "job" (if executed on a
cluster) or as a background process.

```
  ./job.sh
    --project <project_name>
    --bug <bug_id>
    --output_dir <output_dir_path>
    [--help]
```

Where `<project_name>` is a project name (e.g., Chart), `<bug_id>` is the bug id
(e.g., 5), and `<output_dir_path>` is the directory to where the intermediate
scoring files will be written.

#### Output

Under `<output_dir_path>` directory, `jobs.sh` script creates a directory
`<project_name>/<bug_id>` to which two relevant files are written:

* `log.txt` - all debug/info messages produced by the `job.sh` script.
* `scores.csv` - all computed fault localization scores. It looks like:

```
Project,Bug,TestSuite,ScoringScheme,Family,Formula,TotalDefn,KillDefn,HybridScheme,AggregationDefn,Score,ScoreWRTLoadedClasses
Chart,1,developer,first,mbfl,ochiai,tests,exact,none,max,0.0369830466269635,0.26863365739694
Chart,1,developer,first,mbfl,muse,elements,passfail,none,avg,0.0360388869290946,0.261775567111312
Chart,1,developer,first,sbfl,tarantula,tests,,none,,0.000373513726629454,0.0027130906624463
Chart,1,developer,first,sbfl,jaccard,tests,,none,,0.000373513726629454,0.0027130906624463
...
```

All intermediate scoring files can be found in
`<output_dir_path>/<project_name>/<bug_id>/do-full-analysis` and
`<output_dir_path>/<project_name>/<bug_id>/do-previously-studied-flts`. Note
that `<output_dir_path>/<project_name>/<bug_id>/do-full-analysis` is only
generated for real faults.

At the end `job.sh` script compresses the data generated (i.e., the directory
`<output_dir_path>/<project_name>/<bug_id>`) to the file
`<output_dir_path>/<project_name>/<bug_id>/scores.tar.gz`.

## Collect scores

* `collect_scores.sh` script collects the fault localization scores computed by
`job.sh` for each Defects4J fault.

#### Requirements

* D4J_HOME - Must point to the Defects4J installation.

#### Usage

```
  ./collect_scores.sh
    --input_dir <input_dir_path>
    --output_dir <output_dir_path>
```

Where `<input_dir_path>` should point to the output directory of `job.sh` script.
Directory structure should follow the following format:
`<input_dir_path>/<pid>/<bid>/[do-previously-studied-flts|do-full-analysis]/scores.csv`.
Script writes to `<output_dir>` two .csv files for real faults
(`scores_artificial_vs_real.csv` and `scores_real_exploration.csv`) and one .csv
file for artificial faults (`scores_artificial_vs_real.csv`).

